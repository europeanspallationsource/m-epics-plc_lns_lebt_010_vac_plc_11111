# @field PLCNAME
# @type STRING
# PLC name, should be the same as the name in CCDB

# @field IPADDR
# @type STRING
# PLC IP address

# @field S7DRVPORT
# @type INTEGER
# PLC comms port for EPICS s7plc driver connection, should be the same as configured in PLC connection configuration (default = 2000)

# @field MODBUSDRVPORT
# @type INTEGER
# PLC comms port for EPICS modbus driver connection, should be the same as configured in PLC connection configuration (default = 502)

# @field INSIZE
# @type INTEGER
# PLC->EPICS data length (bytes), should match PLC SND block configuration

# @field RECVTIMEOUT
# @type INTEGER
# PLC->EPICS receive timeout (ms), should be longer than frequency of PLC SND block trigger (REQ input)


# Call the EEE module responsible for configuring IOC to PLC comms configuration
requireSnippet(s7plc-comms.cmd, "PLCNAME=$(PLCNAME),IPADDR=$(IPADDR),S7DRVPORT=$(S7DRVPORT), MODBUSDRVPORT=$(MODBUSDRVPORT),INSIZE=$(INSIZE),OUTSIZE=0,BIGENDIAN=1,RECVTIMEOUT=$(RECVTIMEOUT)")

# Load the database defining your EPICS records
dbLoadRecords(database.db, "PLCNAME = $(PLCNAME)")
